package com.ruoyi.common.translate.utils;

import java.lang.reflect.Field;

/**
 * @author zrd
 * @version 1.0
 * @date 2020/12/28 15:57
 */
public class TranUtils {

    /**
     * 描述：将翻译后的值 赋给对应字段
     * 备注：
     * 日期： 10:07 2019/12/5
     * 作者： zrd
     *
     * @param obj   原始实体
     * @param filed 待翻译字段
     * @param value 翻译后的值
     * @return java.lang.Object
     **/
    public static Object taransFilds(Object obj, String filed, String value) {
        try {
            Class<?> c = obj.getClass();
            Field field = c.getDeclaredField(filed);
            //获取字段
            field.setAccessible(true);
            field.set(obj, value);
            //为字段赋值
        } catch (Exception e) {
//            e.printStackTrace();

        }
        return obj;
    }

}
