package com.ruoyi.gen.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.gen.domain.TestCase;
import com.ruoyi.gen.mapper.TestCaseMapper;
import com.ruoyi.gen.service.ITestCaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 测试用例Service业务层处理
 *
 * @author ruoyi
 * @date 2020-12-24
 */
@Service
public class TestCaseServiceImpl extends ServiceImpl<TestCaseMapper, TestCase> implements ITestCaseService {
    @Autowired
    private TestCaseMapper testCaseMapper;


    /**
     * 查询测试用例列表
     *
     * @param testCase 测试用例
     * @return 测试用例
     */
    @Override
    public List<TestCase> selectTestCaseList(TestCase testCase) {
        return testCaseMapper.selectTestCaseList(testCase);
    }


}
